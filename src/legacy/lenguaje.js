import { getEnvironment } from './navigation';

/**
 * Constructor para cadena de idioma
 * @param {string} LNG_ES Texto en español
 * @param {string} LNG_VA Texto en valenciano
 */
function TxtMultidioma(LNG_ES, LNG_VA) {
  this.LNG_ES = LNG_ES;
  this.LNG_VA = LNG_VA;
}
// Determina si estamos en PRE o PRO
const service = getEnvironment();

/**
 * Carga en un diccionario los textos del CdM en los respectivos idiomas.
 * @returns Diccionario con el texto cargado.
 */
function cargarLenguaje() {
  const dictLng = new Map();
  let cabeceraCas = 'Acceso a Cuadros de Mandos'
  let cabeceraVal = 'Accés als Quadres de Comandament'

  if (service === 'PRE') {
    cabeceraCas += ' PRE';
    cabeceraVal += ' PRE';
  }

  dictLng.set(
    'txt-title',
    new TxtMultidioma(
      `Valencia<span>${cabeceraCas}</span>`,
      `València<span>${cabeceraVal}</span>`,
    ),
  );
  dictLng.set(
    'txt-bienvenida',
    new TxtMultidioma(
      `Bienvenido/a: <span id="user_txt">${Dashboards.context.user}`,
      `Benvingut/a: <span id="user_txt">${Dashboards.context.user}`,
    ),
  );
  dictLng.set(
    'txt-valencia',
    new TxtMultidioma('Ayuntamiento de Valencia', 'Ajuntament de València'),
  );
  dictLng.set('idLogoOff', new TxtMultidioma('Cerrar sesión', 'Tancar sessió'));
  dictLng.set(
    'sin-paneles',
    new TxtMultidioma(
      'No tiene permisos para acceder a los cuadros de mando. Para más información, por favor, contacte con el Servicio de Ciudad Inteligente',
      'No té permís per a accedir als quadres de comandament. Per a més informació, per favor, contacte amb el Servici de Ciutat Intel·ligent',
    ),
  );
  dictLng.set('filter_todos', new TxtMultidioma('Todos', 'Tots'));
  dictLng.set(
    'filter_estrategico',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament'),
  );
  dictLng.set('filter_tactico', new TxtMultidioma('Servicio', 'Servici'));
  dictLng.set(
    'filter_operacional',
    new TxtMultidioma('Específicos', 'Específics'),
  );
  // ALCALDÍA
  dictLng.set(
    'span-tipo-alcaldia',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament'),
  );
  dictLng.set(
    'span-ambito-alcaldia',
    new TxtMultidioma('Alcaldía', 'Alcaldia'),
  );
  dictLng.set(
    'span-descripcion-alcaldia',
    new TxtMultidioma(
      'Indicadores, gráficos e informes para una gestión de alto nivel de la ciudad',
      "Indicadors, gràfics i informes per a una gestió d'alt nivell de la ciutat",
    ),
  );
  // CIUDAD
  dictLng.set(
    'span-tipo-ciudad',
    new TxtMultidioma('Servicio', 'Servici')
  );

  dictLng.set(
    'span-ambito-ciudad',
    new TxtMultidioma('Ciudad', 'Ciutat')
  );

  dictLng.set(
    'span-descripcion-ciudad',
    new TxtMultidioma(
      'Gráficos con datos históricos para la gestión de la ciudad',
      'Gràfics amb dades històriques per a la gestió de la ciutat'
    )
  );
  // ECONOMICO
  dictLng.set(
    'span-tipo-economico',
    new TxtMultidioma('Servicio', 'Servici')
  );

  dictLng.set(
    'span-ambito-economico',
    new TxtMultidioma('Gestión Económica', 'Gestió Econòmica')
  );

  dictLng.set(
    'span-descripcion-economico',
    new TxtMultidioma(
      'Indicadores, gráficos e informes para la gestión de los servicios municipales',
      'Indicadors, gràfics i informes per a la gestió dels servicis municipals'
    )
  );
  
  // INDICADORES
  dictLng.set(
    'span-tipo-indicadores',
    new TxtMultidioma('Servicio', 'Servici')
  );

  dictLng.set(
    'span-ambito-indicadores',
    new TxtMultidioma(
      'Indicadores <span class="explicacion">(URBO)</span>',
      'Indicadors <span class="explicacion">(URBO)</span>'
    )
  );

  dictLng.set(
    'span-descripcion-indicadores',
    new TxtMultidioma(
      'Indicadores clave para la medición del estado de la ciudad',
      "Indicadors clau per al mesurament de l'estat de la ciutat"
    )
  );
  // UNIFICADO
  dictLng.set(
    'span-tipo-unificado',
    new TxtMultidioma('Servicio', 'Servici')
  );

  dictLng.set(
    'span-ambito-unificado',
    new TxtMultidioma('Unificado', 'Unificat')
  );

  dictLng.set(
    'span-descripcion-unificado',
    new TxtMultidioma(
      'Indicadores, gráficos e informes de gestión de la Movilidad',
      "Indicadors, gràfics i informes per al mesurament de l'estat de la ciutat"
    )
  );
  //MOVILIDAD
  dictLng.set(
    'span-tipo-movilidad',
    new TxtMultidioma('Específico', 'Específic')
  );

  dictLng.set(
    'span-ambito-movilidad',
    new TxtMultidioma('Movilidad', 'Mobilitat')
  );

  dictLng.set(
    'span-descripcion-movilidad',
    new TxtMultidioma(
      'Datos de movilidad de diversas fuentes',
      'Dades de mobilitat de diverses fonts'
    )
  );
  //MOVILIDAD ANT
  dictLng.set(
    'span-tipo-movilidad_ant',
    new TxtMultidioma('Específico', 'Específic')
  );

  dictLng.set(
    'span-ambito-movilidad_ant',
    new TxtMultidioma('Movilidad (Antiguo)', 'Mobilitat (Antic)')
  );

  dictLng.set(
    'span-descripcion-movilidad_ant',
    new TxtMultidioma(
      'Indicadores, gráficos e informes de gestión de la Movilidad',
      'Indicadors, gràfics i informes de gestió de la Mobilitat'
    )
  );
  //MEDIO AMBIENTE
  dictLng.set(
    'span-tipo-medioambiente',
    new TxtMultidioma('Específico', 'Específic')
  );

  dictLng.set(
    'span-ambito-medioambiente1',
    new TxtMultidioma(
      'Medio',
      'Medi'
    )
  );
  dictLng.set(
    'span-ambito-medioambiente2',
    new TxtMultidioma(
      'ambiente',
      'ambient'
    )
  );
  dictLng.set(
    'span-descripcion-medioambiente',
    new TxtMultidioma(
      'Sensores medioambientales instalados en la ciudad',
      'Sensors mediambientals instal·lats a la ciutat'
    )
  );
  // WIFI
  dictLng.set(
    'span-tipo-wifi',
    new TxtMultidioma('Específico', 'Específic')
  );

  dictLng.set(
    'span-ambito-wifi',
    new TxtMultidioma('WiFi', 'WiFi')
  );

  dictLng.set(
    'span-descripcion-wifi',
    new TxtMultidioma(
      'Puntos WiFi instalados en la ciudad',
      'Punts Wifi instal·lats a la ciutat'
    )
  );

  // SENSORES
  dictLng.set(
    'span-tipo-sensores',
    new TxtMultidioma('Específico', 'Específic')
  );

  dictLng.set(
    'span-ambito-sensores',
    new TxtMultidioma('Sensores', 'Sensors')
  );

  dictLng.set(
    'span-descripcion-sensores',
    new TxtMultidioma(
      'Sensores IoT instalados en la ciudad',
      'Sensors IoT instal·lats a la ciutat'
    )
  );

  // VEHÍCULO ELÉCTRICO
  dictLng.set(
    'span-tipo-velectrico',
    new TxtMultidioma('Específico', 'Específic')
  );

  dictLng.set(
    'span-ambito-velectrico',
    new TxtMultidioma('Vehículo Eléctrico', 'Vehicle Elèctric')
  );

  dictLng.set(
    'span-descripcion-velectrico',
    new TxtMultidioma(
      'Cargadores de vehículo eléctrico municipales',
      'Carregadors de vehicle elèctric municipals'
    )
  );

  // Administración
  dictLng.set(
    'span-tipo-administracion',
    new TxtMultidioma('Específico', 'Específic')
  );
  dictLng.set(
    'span-ambito-administracion',
    new TxtMultidioma(
      'Desarrollo Pentaho',
      'Desenrotllament Pentaho'
    )
  );

  dictLng.set(
    'span-descripcion-administracion',
    new TxtMultidioma(
      'Panel de administración de Pentaho',
      "Panell d'administració de Pentaho"
    )
  );

  // SigVAL
  dictLng.set(
    'span-tipo-sigval',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-sigval',
    new TxtMultidioma(
      'SigVAL',
      'SigVAL'
    )
  );
  dictLng.set(
    'span-descripcion-sigval',
    new TxtMultidioma(
      'Mapas con información interna para la gestión de los servicios municipales',
      "Mapes amb informació interna per a la gestió dels servicis municipals"
    )
  );

  // Geoportal
  dictLng.set(
    'span-tipo-geoportal',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-geoportal',
    new TxtMultidioma(
      'Geoportal',
      'Geoportal'
    )
  );
  dictLng.set(
    'span-descripcion-geoportal',
    new TxtMultidioma(
      'Mapas con información pública',
      "Mapes amb informació pública"
    )
  ); 
  
  // SCC
  dictLng.set(
    'span-tipo-scc',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-scc',
    new TxtMultidioma(
      'SCC',
      'SCC'
    )
  );
  dictLng.set(
    'span-descripcion-scc',
    new TxtMultidioma(
      'Sugerencias y Comunicaciones de los Ciudadanos',
      "Suggeriments i Comunicacions dels Ciutadans"
    )
  );

  dictLng.set(
    'rights',
    new TxtMultidioma(
      '2024 Ajuntament de València | Servicio de Ciudad Inteligente',
      '2024 Ajuntament de València | Servici de Ciutat Intel·ligent',
    ),
  );

  // MAPA POBLACION
  dictLng.set(
    'span-tipo-mapa-poblacion',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-mapa-poblacion',
    new TxtMultidioma(
      'Población e inspección DANA',
      'Població i inspecció DANA'
    )
  );
  dictLng.set(
    'span-descripcion-mapa-poblacion',
    new TxtMultidioma(
      'Mapa con datos de población, y edificios con personas solas',
      "Mapa amb dades de població, i edificis amb persones soles"
    )
  );

  // INCIDENCIAS
  dictLng.set(
    'span-tipo-incidencias',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-incidencias',
    new TxtMultidioma(
      'Incidencias en vía pública',
      'Incidències en via pública'
    )
  );
  dictLng.set(
    'span-descripcion-incidencias',
    new TxtMultidioma(
      'Mapa con incidencias de ciudadanos en vía pública',
      "Mapa amb incidències de ciutadans en via pública"
    )
  );

  // INCIDENCIAS DETALLE
  dictLng.set(
    'span-tipo-incidencias-detalle',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-incidencias-detalle',
    new TxtMultidioma(
      'Incidencias Vía Pública DANA <span>(Detalle)</span>',
      'Incidències Via Pública DANA <span>(Detall)</span>'
    )
  );
  dictLng.set(
    'span-descripcion-incidencias-detalle',
    new TxtMultidioma(
      'Incidencias en vía pública y padrón por barrios en el último año',
      "Incidències en via pública i padró per barris en l'últim any"
    )
  );

  // ESTADISTICAS DANA
  dictLng.set(
    'span-tipo-estadisticas-dana',
    new TxtMultidioma('Ayuntamiento', 'Ajuntament')
  );
  dictLng.set(
    'span-ambito-estadisticas-dana',
    new TxtMultidioma(
      'Estadística DANA',
      'Estadística DANA'
    )
  );
  dictLng.set(
    'span-descripcion-estadisticas-dana',
    new TxtMultidioma(
      'Datos estadísticos para la gestión de la DANA',
      "Dades estadístiques per a la gestió de la DANA"
    )
  );

  dictLng.set(
    'rights',
    new TxtMultidioma(
      '2024 Ajuntament de València | Servicio de Ciudad Inteligente',
      '2024 Ajuntament de València | Servici de Ciutat Intel·ligent',
    ),
  );
  return dictLng;
}

/**
 * Implementación del cambio del idioma del texto al idioma que viene por parámetros.
 * @param {string} lang Idioma al que se va a cambiar.
 * @param {dict} dictLng Diccionario con los textos a cambiar.
 */
function actualizarIdioma(lang, dictLng) {
  const htmlElement = document.querySelector('html');
  const currentLang = htmlElement.getAttribute('lang');

  if (currentLang !== lang) {
    htmlElement.setAttribute('lang', lang);
    htmlElement.setAttribute('xml:lang', lang);
  }

  try {
    dictLng.forEach((value, key) => {
      document.getElementById(key).innerHTML =
        lang === 'es-ES' ? value.LNG_ES : value.LNG_VA;
    });
  } catch (error) {
    console.error(`Error al cambiar lenguaje a ${lang}: ${error}`);
  }
  const elemLang = document.querySelector('.idioma');
  elemLang.className =
    lang === 'es-ES' ? 'idioma idiomaES' : 'idioma idiomaVAL';
}
/**
 * Añade la funcionalidad de cambiar el lenguaje a dos botones.
 */
export function cambiarLenguaje() {
  const btnCas = document.getElementById('btn-idioma-cas');
  const btnVal = document.getElementById('btn-idioma-val');
  const dictLng = cargarLenguaje();
  window.document.title = (service == 'PRE') ? 'Portada PRE' : 'Portada';

  btnCas.addEventListener('click', () => actualizarIdioma('es-ES', dictLng));
  btnVal.addEventListener('click', () => actualizarIdioma('ca-ES', dictLng));
}
