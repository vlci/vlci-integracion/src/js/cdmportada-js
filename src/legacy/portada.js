import { cambiarLenguaje } from './lenguaje';
import { getEnvironment } from './navigation';


/**
 * Columnas de la consulta a la BBDD.
 */
const COLUMNA_ROL = 0;

/**
 * Constructor para almacenar el id y la url de un cdm
 * @param {string} idList Id asociado al id en la lista html
 * @param {string} idGroup Id asociado al grupo al que pertenece el cdm
 * @param {string} url URL que lleva al cdm correspondiente
 */
function DatosCdM(idList, idGroup, url) {
  this.idList = idList;
  this.idGroup = idGroup;
  this.url = url;
}

/**
 * Cada uno de ellos se corresponde con un cuadro de mando, que tiene su ID y su URL.
 * Para buscar un elemento en el diccionario, basta con poner cdms.XXXX, siendo XXXX el cdm.
 */

const service = getEnvironment() == 'PRE' ? 'sc_valencia_pre' : 'sc_valencia'

const RUTA_VLCI =
  `https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3A${service}%3A`;

const RUTA_URBO = 'https://urbo.vlci.valencia.es/panel/';
const cdms = {
  Alcaldia: new DatosCdM(
    'alcaldia',
    'estrategico',
    `${RUTA_VLCI}Alcaldia%3AInicio.wcdf/generatedContent`,
  ),
  Ciutat: new DatosCdM(
    'ciudad',
    'tactico',
    `${RUTA_VLCI}Ciutat%3AQuadre_de_Comandament.wcdf/generatedContent`,
  ),
  Economico: new DatosCdM(
    'economico',
    'tactico',
    `${RUTA_VLCI}GE%3AQuadre_de_GE.wcdf/generatedContent`,
  ),
  Indicadores: new DatosCdM(
    'indicadores',
    'tactico',
    `${RUTA_URBO}indicadores-vista-general`,
  ),
  Unificado: new DatosCdM(
    'unificado',
    'tactico',
    `${RUTA_VLCI}SmartCity%3AQuadre_Unificado.wcdf/generatedContent`,
  ),
  Movilidad: new DatosCdM(
    'movilidad',
    'operacional',
    `${RUTA_VLCI}Alcaldia_Mobilitat%3AInicio.wcdf/generatedContent`,
  ),
  Movilidad_ant: new DatosCdM(
    'movilidad_ant',
    'operacional',
    `${RUTA_VLCI}Mobilitat%3AMobilitat.wcdf/generatedContent`,
  ),
  MedioAmbiente: new DatosCdM(
    'medioambiente',
    'operacional',
    `${RUTA_URBO}medio-ambiente-estado-general`,
  ),
  Wifi: new DatosCdM(
    'wifi',
    'operacional',
    `${RUTA_URBO}wifi-estado-general`,
  ),
  Sensores: new DatosCdM(
    'sensores',
    'operacional',
    `${RUTA_URBO}monitorizacion-sensores`,
  ),
  vElectrico: new DatosCdM(
    'v-electrico',
    'operacional',
    'https://www.valencia.es/apps/gestionrecarga/etrapp/WebApp.aspx?webappurl=WebApps.aspx#',
  ),
  Administracion: new DatosCdM(
    'administracion',
    'operacional',
    'https://cdm.vlci.valencia.es/pentaho/Home'
  ),
  Sigval: new DatosCdM(
    'sigval',
    'estrategico',
    'https://sigval.valencia.es/portal'
  ),
  Geoportal: new DatosCdM(
    'geoportal',
    'estrategico',
    'https://geoportal.valencia.es/'
  ),
  SCC: new DatosCdM(
    'scc',
    'estrategico',
    'https://sigval.valencia.es/portal/apps/opsdashboard/index.html#/882abb9a26954179be766dc45f3d620b'
  ),
  MapaPoblacion: new DatosCdM(
    'mapa-poblacion',
    'estrategico',
    'https://sigval.valencia.es/portal/apps/opsdashboard/index.html#/79eaeed8b419495a882143eb9fc0c5f9'
  ),
  Incidencias: new DatosCdM(
    'incidencias',
    'estrategico',
    'https://sigval.valencia.es/portal/apps/opsdashboard/index.html#/882abb9a26954179be766dc45f3d620b'
  ),
  IncidenciasDetalle: new DatosCdM(
    'incidencias-detalle',
    'estrategico',
    'https://sigval.valencia.es/portal/apps/opsdashboard/index.html#/707da42b360b4c3a81c86308a3f09b49'
  ),
  EstadisticasDana: new DatosCdM(
    'estadisticas-dana',
    'estrategico',
    'https://apps.valencia.es/datosestadisticos/dana/index.php'
  )
};

/**
 * Diccionaro con los roles existentes en la BBDD.
 * Cada entidad se compone de su ID (nombre del rol tal cual está guardado en la BBDD) y un array con los cdms a los que tiene acceso.
 */
const dictRol = new Map();
const allCdms = [
  cdms.Ciutat,
  cdms.Economico,
  cdms.Movilidad,
  cdms.Movilidad_ant,
  cdms.Unificado,
  cdms.MedioAmbiente,
  cdms.Sensores,
  cdms.Wifi,
  cdms.Indicadores,
  cdms.Alcaldia,
  cdms.vElectrico,
  cdms.Sigval,
  cdms.Geoportal,
  cdms.SCC,
  cdms.Administracion,
  cdms.Incidencias,
  cdms.IncidenciasDetalle,
  cdms.MapaPoblacion,
  cdms.EstadisticasDana
];
dictRol.set('DBPentDeveloper', allCdms);
dictRol.set('DBPentAdmins', allCdms);
dictRol.set('funcionario_movilidad', [cdms.Ciutat, cdms.Movilidad_ant, cdms.Sigval, cdms.Geoportal]);
dictRol.set('director_gestion_datos', [cdms.Alcaldia, cdms.Economico, cdms.Movilidad_ant, cdms.Incidencias, cdms.MapaPoblacion, cdms.EstadisticasDana]);
dictRol.set('funcionario_oci', [cdms.Ciutat, cdms.Alcaldia, cdms.Movilidad, cdms.Geoportal, cdms.Sigval, cdms.SCC, cdms.MedioAmbiente, cdms.Sensores, cdms.Incidencias, cdms.IncidenciasDetalle, cdms.MapaPoblacion, cdms.EstadisticasDana])
dictRol.set('funcionario_oci_wifi', [cdms.Ciutat, cdms.Alcaldia, cdms.Movilidad, cdms.Geoportal, cdms.Sigval, cdms.SCC, cdms.MedioAmbiente, cdms.Sensores, cdms.Wifi, cdms.Incidencias, cdms.IncidenciasDetalle, cdms.MapaPoblacion, cdms.EstadisticasDana])
dictRol.set('jefe_servicio', [cdms.Geoportal, cdms.Sigval]);
dictRol.set('jefe_servicio_medioambiente', [cdms.Ciutat, cdms.MedioAmbiente, cdms.Geoportal, cdms.Sigval]);
dictRol.set('jefe_servicio_movilidad', [cdms.Ciutat, cdms.Movilidad_ant, cdms.Geoportal, cdms.Sigval]);
dictRol.set('politico_gobierno', [cdms.Alcaldia, cdms.Incidencias, cdms.MapaPoblacion, cdms.EstadisticasDana]);
dictRol.set('politico_oposicion', [cdms.Ciutat]);
dictRol.set('secretario', [cdms.Ciutat, cdms.Alcaldia, cdms.Geoportal, cdms.Sigval, cdms.SCC, cdms.Incidencias, cdms.IncidenciasDetalle, cdms.MapaPoblacion, cdms.EstadisticasDana]);
dictRol.set('funcionario_sertic', [cdms.Ciutat, cdms.Alcaldia, cdms.Sigval, cdms.Geoportal, cdms.SCC, cdms.Incidencias, cdms.IncidenciasDetalle, cdms.MapaPoblacion, cdms.EstadisticasDana]);
dictRol.set('jefe_servicio_sertic', [cdms.Ciutat, cdms.Alcaldia, cdms.Sigval, cdms.Geoportal, cdms.SCC, cdms.Incidencias, cdms.IncidenciasDetalle, cdms.MapaPoblacion, cdms.EstadisticasDana]);

/**
 * Método para mostrar/ocultar los paneles a los que se tiene acceso (según el usuario).
 * @param {array} datos Rol del usuario
 */
export function postFetchPermisos(datos) {
  if (datos.resultset.length > 0) {
    const cdmsVisibles = dictRol.get(datos.resultset[0][COLUMNA_ROL]);
    if (cdmsVisibles.length > 0) {
      const cdmsVisiblesSet = new Set(cdmsVisibles.map((cdm) => cdm.idList));
      // Recorro todos los ids disponibles para ocultar aquellos que no sean visibles.
      Object.values(cdms).forEach((cdm) => {
        const elem = document.getElementById(cdm.idList);
        if (!cdmsVisiblesSet.has(cdm.idList)) {
          elem.style.display = 'none';
        } else {
          // Los que sean visibles les añado URL y muestro su filtro.
          elem.addEventListener('click', () => window.open(cdm.url, '_blank'));
          document.getElementById(`filter_${cdm.idGroup}`).style.display =
            'flex';
        }
      });
      return;
    }
    Object.values(cdms).forEach((cdm) => {
      document.getElementById(cdm.idList).style.display = 'none';
    });
    document.getElementById('filter_todos').style.display = 'none';
    document.getElementById('sin-paneles').style.display = 'block';
  }
}
/**
 * Llamadas iniciales. Asigna las funciones JS a los botones, además de asignar el lenguaje inicial.
 */
function initCalls() {
  const completo = document.getElementById('completo');
  const todos = document.getElementById("filter_todos");
  const estrategico = document.getElementById('filter_estrategico');
  const tactico = document.getElementById('filter_tactico');
  const operacional = document.getElementById('filter_operacional');

  todos.classList.add("selected");

  todos.addEventListener('click', () => {
    completo.removeAttribute('class');
    todos.classList.add("selected");

    estrategico.classList.remove("selected");
    tactico.classList.remove("selected");
    operacional.classList.remove("selected");
  });

  estrategico.addEventListener('click', () => {
    completo.removeAttribute('class');
    completo.classList.add('estrategico');
    estrategico.classList.add("selected");

    todos.classList.remove("selected");
    tactico.classList.remove("selected");
    operacional.classList.remove("selected");
  });

  tactico.addEventListener('click', () => {
    completo.removeAttribute('class');
    completo.classList.add('tactico');
    tactico.classList.add("selected");

    todos.classList.remove("selected");
    estrategico.classList.remove("selected");
    operacional.classList.remove("selected");
  });
  operacional.addEventListener('click', () => {
    completo.removeAttribute('class');
    completo.classList.add('operacional');
    operacional.classList.add("selected");

    todos.classList.remove("selected");
    estrategico.classList.remove("selected");
    tactico.classList.remove("selected");
  });

  cambiarLenguaje();
  document.getElementById('btn-idioma-cas').click();
}

initCalls();
