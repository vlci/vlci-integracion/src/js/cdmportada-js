const path = require('path');
module.exports = {
    devServer: {
        port: 8888,
        client: {
            overlay: false,
          }
    },
    devtool: 'eval-cheap-source-map',
    mode: 'production',
    entry: { legacy: './src/legacy/index.js' },
    output: {
        path: path.resolve(__dirname, 'dist/js'),
        filename: '[name].js',
        library: "[name]",
        libraryTarget: "umd",
        clean: true,
        publicPath: '/dist/js/'
    },
};